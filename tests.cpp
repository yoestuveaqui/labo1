#include "lista_alumnos.h"
#include <iostream>

// para compilar:$ g++ -g tests.cpp lista_alumnos.cpp -o lista_alumnos
// para ejecutar con valgrind:$ valgrind --leak-check=full -v ./lista_alumnos

void test_lista_vacia()
{
  cout<<"test_lista_vacia()"<<std::endl; 		
  ListaAlumnos l;
  cout<<"LONGITUD: "<<l.longitud()<<std::endl<<std::endl;
}

void test_lista_con_un_elemento()
{
  std::cout<<"test_lista_con_un_elemento()"<<std::endl<<std::endl; 		
  ListaAlumnos l;

  Alumno alumno(78403, "Pikachu");

  l.agAdelante( alumno );
  std::cout<<l<<std::endl;
}

void test_lista_con_dos_elementos()
{
  //cout<<"test_lista_con_dos_elementos()"<<std::endl<<std::endl;	
  ListaAlumnos l;

  Alumno alumno(78403, "Pikachu");
  Alumno jonh(007,"Jonh");
	
  l.agAdelante( alumno );
  l.agAdelante(jonh);
  
  cout<<l<<std::endl;
  //cout<<"----------YA SALI------------";
}

void test_operator()
{
	cout<<"test_operator_=()"<<std::endl;
	ListaAlumnos l1;
	Alumno alumno(78403, "Pikachu");
    Alumno jonh(007,"Jonh");
    l1.agAdelante( alumno );
    l1.agAdelante(jonh);
    
    ListaAlumnos copia = l1;
    l1.eliminar(1);
    //cout<<"ORIGINALl1<<std::endl;
    cout<<"COPIA"<<std::endl<<copia<<std::endl;
}

void test_copia(){
	ListaAlumnos l1;
	Alumno alumno(78403, "Pikachu");
    Alumno jonh(007,"Jonh");
    l1.agAdelante( alumno );
    l1.agAdelante(jonh);
    
    ListaAlumnos copia = ListaAlumnos(l1);
}
        
	

int main()
{
  //test_lista_vacia();
  //test_lista_con_un_elemento();
  //test_lista_con_dos_elementos();
  test_operator();
  //test_copia();

  return 0;
}
