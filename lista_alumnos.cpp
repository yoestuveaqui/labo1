#include "lista_alumnos.h"

ListaAlumnos::ListaAlumnos()
  : len(0), prim(NULL), fin(NULL) {
}

ListaAlumnos::ListaAlumnos(const ListaAlumnos& l){
	if(l.len != 0){
		Nodo* recorredor = prim;
		while(recorredor != NULL){
			int lu = (recorredor-> alumno).LU;
			string name = (recorredor-> alumno).nombre;
			Alumno nuevo(lu,name);
			agAtras(nuevo);
			recorredor = recorredor -> sig;
		}
	}
}

ListaAlumnos::~ListaAlumnos() {
	while(len != 0){
		eliminar(1);
	}
}

ListaAlumnos& ListaAlumnos::operator=(const ListaAlumnos& aCopiar){
	if(aCopiar.len == 0){
		return *this;
	}else{
		Nodo* proximo = aCopiar.prim;
		while(proximo->sig != NULL){
			agAtras(proximo->alumno);
			proximo = proximo->sig;
		}
	}
	return *this;
}

int ListaAlumnos::longitud() {
  return len;
}

Alumno& ListaAlumnos::iesimo(Nat i){
	if (longitud() == 0){
		Alumno alumno_inexistente;
		Alumno& ref = alumno_inexistente;
		return ref; 
	}else{
		int i = 1;
		Nodo* ptr = prim;
		while(i != longitud()){
			ptr = ptr-> sig;
			i++;
		}
		Alumno& ref = ptr -> alumno;
		return ref; 
	}
}

void ListaAlumnos::agAdelante(Alumno& elem) {
  Nodo* nuevo = new Nodo;
  nuevo->alumno = elem;
  nuevo->sig = prim ;

  len++;
  prim = nuevo;
  if(len == 1){
    fin = nuevo;
    prim->sig=NULL;
  }
}

void ListaAlumnos::agAtras(Alumno& elem){
	Nodo* nuevo = new Nodo;
	nuevo -> alumno = elem;
	nuevo -> sig = NULL;
	
	fin-> sig = nuevo;
	fin = nuevo;
	len++;
	if(len==1) prim = nuevo;
}

void ListaAlumnos::eliminar(Nat i){
	Nodo* recorredor = prim;
	if(i==1 || i == len){
		if(i==1){
			prim = recorredor->sig;
			delete prim;
			len--;
		}else{
			int j = 1;
			while(j< len){
				recorredor = recorredor->sig;
				j++;
			}
			delete fin;
			fin = recorredor;
		}
	}else{
		int j = 1;
		while(j< len){
			recorredor = recorredor->sig;
			j++;	
		}
		Nodo* borrar = recorredor -> sig;
		recorredor -> sig = borrar -> sig;
		delete borrar;
		len--;
	}	
}

void ListaAlumnos::mostrar(ostream& o){
	o<<"----------CIMA--------"<<std::endl;
	Nodo* aux = prim;
	while(aux != NULL){
		o<< aux->alumno<<std::endl;
		aux= aux->sig; 
	}
	o<<"----------FONDO-------"<<std::endl;
}
// Completar
